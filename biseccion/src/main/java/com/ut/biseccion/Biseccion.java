/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ut.biseccion;

import com.ut.dto.IteracionDTO;
import com.ut.util.Function;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author User
 */
public class Biseccion {
    
    private final List<IteracionDTO> listIter = new ArrayList<>();

    public List<IteracionDTO> getIteraciones(){
        return listIter;
    }
    
    public double raiz(Function f, double xa, double xu, double error, int nMax) {
        double raiz = Double.NaN;// Valor por defecto 
        double c = xa;
        int iter = 0;
        if (f.evaluate(xa) * f.evaluate(xu) < 0) {
            while (error <= Math.abs(f.evaluate(c)) && iter < nMax) {
                c = (xa + xu) / 2;
                listIter.add(new IteracionDTO(iter, xa, xu, c));
                if (f.evaluate(xa) * f.evaluate(c) < 0) {
                    xu = c;     
                }else{
                    xa = c;
                }
                iter++;
            }
        }
        if (iter < nMax) {
            raiz = c;
        }
        
        if (f.evaluate(xa) * f.evaluate(xu) > 0) {
            JOptionPane.showMessageDialog(null, "No hay cambio de signo\nf(Xa)f(Xu)>0","Error",JOptionPane.ERROR_MESSAGE);
             raiz = Double.NaN;
             listIter.clear();
            
        }
        return raiz;
    }

    public void borrarColeccion() {
        listIter.clear();
    }
    
}
