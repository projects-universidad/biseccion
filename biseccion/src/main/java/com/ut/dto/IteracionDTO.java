/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ut.dto;

/**
 *
 * @author User
 */
public class IteracionDTO {
    
    private int iteracion;
    private double xa;
    private double xu;
    private double xr;

    public IteracionDTO(int iteracion, double xa, double xu, double xr) {
        this.iteracion = iteracion;
        this.xa = xa;
        this.xu = xu;
        this.xr = xr;
    }

    public int getIteracion() {
        return iteracion;
    }

    public void setIteracion(int iteracion) {
        this.iteracion = iteracion;
    }

    public double getXa() {
        return xa;
    }

    public void setXa(double xa) {
        this.xa = xa;
    }

    public double getXu() {
        return xu;
    }

    public void setXu(double xu) {
        this.xu = xu;
    }

    public double getXr() {
        return xr;
    }

    public void setXr(double xr) {
        this.xr = xr;
    }
    
}
