/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ut.util;

import javax.swing.JOptionPane;
import org.nfunk.jep.JEP;

/**
 *
 * @author User
 */
public class Function {
    JEP j = new JEP();

    public Function(String def) {
        j.addVariable("x", 0);
        j.addStandardConstants();
        j.addStandardFunctions();
         j.setImplicitMul(true);
        j.parseExpression(def);
        if (j.hasError()) {
            JOptionPane.showMessageDialog(null, " error  al convertir la funcion");
        }
    }
    
    public double evaluate(double x){
        double r;
        j.addVariable("x",x);
        r=j.getValue();
        if (j.hasError()) {
            JOptionPane.showMessageDialog(null, " error  al convertir la funcion");
        }
        return r;     
    }
}
